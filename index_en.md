---
title: Manual for the use of the factory
---
Manual proposed for the workshop "Using web development tools to make books" during the CSDH-SCHN 2021 conference.

## Theoretical preamble
Making a book is a complex process. It requires specific skills, methods and tools. Classically built around two tools that are a word processor and a desktop publishing software, the publishing chain is not easily accessible to everyone. For several years now, publishing structures have been experimenting with other ways of working on the design and production of books. They are inspired by the methods and tools of web development, favouring a modular logic and horizontal functioning. It is now possible to design, produce and distribute quality printed and digital books with open source/free tools that respect standards.

What this means:

- abandoning traditional tools such as word processors and desktop publishing software ;
- using free software and programs, and open (and flexible) platforms;
- gaining scalability: being able to change _modules_ easily;
- the problem: the technical jump for people like you and me!

The principle of single source publishing allows to produce different forms/artifacts/formats and brings many advantages:

- not having to maintain different files of the same content ;
- to be able to use the same tool to produce different forms;
- to foresee different scenarios according to the output formats (for example a different image quality for the printed version);
- Maintain control over the source files (in the case of a modular factory).

> Making [_fabriquer_ in french] means first of all to manipulate and divert something that is part of the given, to change it into an artifact and to turn it into a practical application.  
> Vilèm Flusser, _Petite philosophie du design_, p. 58

This vision of a factory (_fabrique_ in french) where what counts is learning is doubly interesting: by its malleable nature and its need to adapt, by its reflexive nature and this bilateral relationship (this constant dialogue between man and machine).
We acquire knowledge and practice by using it, which goes beyond the simple "I press a button to trigger an action".

---

## Prerequisites
Creation of two accounts:

- [GitLab](https://gitlab.com/users/sign_up): to host the contents and generate the digital versions;
- [Forestry.io](https://app.forestry.io/signup): to interact with the contents.

---

## How the factory works
Each modification generates the whole book, the book being a web site in two versions:

- a _web_ version organized in several pages, with a design adapting to the size of the screens ;
- a _printable_ version with pages.

The generator performs the following operations:

- transformation of the Markdown source files and their header in YAML/TOML format into HTML files;
- organize the files according to the specified parameters.

![](fabrique-en.png)

Operation in several points (see diagram above):

1. the _editing_ of the contents is done via an interface, with input masks or forms as you can know with tools like WordPresse for example. No need for major technical knowledge, just a little practice (which we will do for two days);
2. it is possible to _predisplay_ at any time, even before saving or publishing, and this only for you (nobody else sees this preview);
3. every change in the interface modifies files that are kept on a code editing platform, GitLab. The files are _synchronized_ and _saved_. Usually reserved for developers, this platform is connected to the editing interface;
4. each time a modification is recorded, the publication is _generated_ via the GitLab platform and the programs installed on it, the files are _transformed_ into HTML files that form a website, a PDF file or an EPUB file;
5. the GitLab platform also _hosts_ these web publications, in a few seconds the versions of the publication or book are available online.

---

## User's manual

### Modify a page
To modify a page you must first access the administration interface.

In the side menu you can select a page: for example a chapter in the `Chapters` folder, or the about page.

The editing interface is separated into two areas: the page information (also called metadata) and the body text.

![](edition-1.png)

At any time you can preview the changes with the `eye` logo at the top right:

![](edition-2.png)

A series of text formatting buttons allow you to easily edit the text:

![](edition-4.png)

As soon as you make a change, the `Save` button pops up and the words "unsaved changes" appear:

![](edition-3.png)

You just have to save, when the circle on the top left becomes green again it means that all GitLab has received the changes and that it generates your final publication:

![](edition-5.png)

### Create a page
In the sidebar select `Chapters` then click on "Create new" then on "Chapters" :

![](creer.png)

#### Metadata (title, etc.)
You need to fill in several fields in the _metadata_, for now at least you have:

- the title ;
- the chapter number: you must not leave this parameter at zero!
- the subtitle, such as "Chapter 1" ;
- possibly a header image.

![](creer-meta.png)

**If a chapter does not have a number it will not be visible.**

#### Recording
Once you have added the metadata (title, etc.), you need to save your document.
The save option is located at the top right of the interface:

![](creer-enr.png)

Attention by default all new pages are in _draft_ mode:

![](creer-draft.png)

If you want this page to be visible online, you have to switch off the _draft_ mode:

![](creer-save.png)

Once the recording is done, the "Save" button turns grey, it is saved and published. Bravo !

![](creer-publish.png)

**Warning: the created page will only be visible a few moments later, 1 or 2 minutes at the most.

#### Specific markup
You can manage several specific parameters:

##### Non-breaking spaces
To add a non-breaking space use the HTML code `&nbsp;`, example: `Ho a cat&nbsp;!`.

### Modify the book parameters
Several choices are offered to set up your publication, via "Settings".

- title of the publication ;
- subtitle ;
- author or authoress ;
- license ;
- publishing house.

### Modify the formatting
A few formatting options are available for the print version:

- cover image;
- format of the printable version (portrait or landscape) ;
- display or not the crop marks;
- text alignment ;
- paragraph indentation ;
- back cover ;
- etc.
