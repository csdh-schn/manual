---
title: Manuel d'utilisation de la fabrique
---
Manuel proposé pour l'atelier "Utiliser les outils du développement web pour fabriquer des livres" durant le colloque CSDH-SCHN 2021.

## Préambule théorique
La fabrication d'un livre est un processus complexe. Il requiert des compétences, des méthodes et des outils spécifiques. Classiquement construite autour de deux outils que sont un traitement de texte et un logiciel de PAO, la chaîne d'édition n'est pas facilement accessible à tous. Depuis plusieurs années, les structures d'édition expérimentent d'autres façons de travailler sur la conception et la réalisation des livres. Elles s'inspirent des méthodes et outils du développement web, privilégiant une logique modulaire et un fonctionnement horizontal. Il est désormais possible de concevoir, produire et distribuer des livres imprimés et numériques de qualité avec des outils open source/libre qui respectent les standards.

Ce que cela signifie :

- abandonner les outils classiques comme les traitements de texte et les logiciels de publication assistée par ordinateur ;
- utiliser des logiciels, programmes libres et plateformes ouvertes (et souples) ;
- gagner en évolutivité : pouvoir changer des _briques_ facilement ;
- le problème : le saut technique pour des personnes comme vous et moi !

Le principe du single source publishing permet de produire différentes formes/artefacts/formats et apporte de nombreux avantages :

- ne pas avoir à maintenir différents fichiers d'un même contenu ;
- pouvoir utiliser un même outil pour produire différentes formes ;
- prévoir des scénarios différents selon les formats de sortie (par exemple une qualité d'image différente pour la version imprimée) ;
- conserver la maîtrise des fichiers sources (dans le cas d'une fabrique modulaire).

> Fabriquer, cela signifie d’abord manipuler et détourner quelque chose qui fait partie du donné, le changer en artefact et le tourner vers l’application pratique.  
> Vilèm Flusser, _Petite philosophie du design_, p. 58

Cette vision d’une fabrique où ce qui compte c’est d’apprendre est doublement intéressante : par sa nature malléable et sa nécessité d’adaptation, par sa nature réflexive et cette relation bilatérale (ce dialogue constant entre l’homme et la machine).
Nous acquérons des connaissances et une pratique en les utilisant, qui dépasse le simple « j’appuie sur un bouton pour déclencher une action ».

---

## Prérequis
Création de deux comptes :

- [GitLab](https://gitlab.com/users/sign_up) : pour héberger les contenus et générer les versions numériques ;
- [Forestry.io](https://app.forestry.io/signup) : pour interagir avec les contenus.

---

## Fonctionnement de la fabrique
Chaque modification génère le livre en entier, le livre étant un site web dans deux versions :

- une version _web_ organisée en plusieurs pages, avec un design s'adaptant à la taille des écrans ;
- une version _imprimable_ paginée.

Le générateur effectue les opérations suivantes :

- transformation des fichiers sources Markdown et de leur entête au format YAML/TOML en fichiers HTML ;
- organisation des fichiers selon les paramètres indiqués.

![](fabrique.png)

Fonctionnement en plusieurs points (voir schéma ci-dessus) :

1. l'_édition_ des contenus se fait via une interface, avec des masques de saisie ou des formulaires comme vous pouvez en connaître avec des outils comme WordPresse par exemple. Pas besoin de connaissances techniques majeures, juste un peu de pratique (ce que nous allons faire pendant deux jours) ;
2. il est possible de _prévisualiser_ à tout moment, avant même d'enregistrer ou de publier, et ce uniquement pour vous (personne d'autre ne voit cette prévisualisation) ;
3. chaque modification dans l'interface modifie des fichiers qui sont conservés sur une plateforme d'édition de code, GitLab. Les fichiers sont _synchronisés_ et _sauvegardés_. D'habitude réservée aux développeurs et aux développeuses, cette plateforme est connectée à l'interface d'édition ;
4. à chaque modification enregistrée la publication est _générée_ via la plateforme GitLab et des programmes qui y sont installés, les fichiers sont _transformés_ en fichiers HTML qui forment un site web, un fichier PDF ou un fichier EPUB ;
5. la plateforme GitLab se charge aussi d'_héberger_ ces publications web, en quelques secondes les versions de la publication ou livre sont disponibles en ligne.

---

## Manuel d'utilisation

### Modifier une page
Pour modifier une page vous devez tout d'abord accéder à l'interface d'administration.

Dans le menu latéral vous pouvez sélectionner une page : par exemple un chapitre dans le dossier `Chapitres`, ou la page À propos.

L'interface d'édition est séparée en 2 espaces : les informations sur la page (aussi appellées métadonnées) et le corps de texte.

![](edition-1.png)

À tout moment vous pouvez prévisualiser les modifications avec le logo `œil` en haut à droite :

![](edition-2.png)

Une série de boutons de mise en forme du texte vous permettent d'éditer facilement le texte :

![](edition-4.png)

Dès que vous effectuez une modification le bouton `Save` se dégrise et la mention "unsaved changes" (modifications non enregistrées) apparaît :

![](edition-3.png)

Vous n'avez plus qu'à enregistrer, lorsque le rond en haut à gauche redevient vert c'est que tout GitLab a bien reçu les modifications et qu'il génère votre publication finale :

![](edition-5.png)

### Créer une page
Dans le panneau latéral sélectionnez `Chapitres` puis cliquez sur "Create new" puis sur "Chapitres" :

![](creer.png)

#### Métadonnées (titre, etc.)
Vous devez renseigner plusieurs champs dans les _métadonnées_, pour le moment à minima vous avez :

- le titre ;
- le numéro de chapitre : vous ne devez pas laisser ce paramètre à zéro&nbsp;!
- le sous-titre, du type "Chapitre 1" ;
- éventuellement une image d'entête.

![](creer-meta.png)

**Si un chapitre ne comporte pas de numéro il ne sera pas visible.**

#### Enregistrement
Une fois que vous avez ajouté les métadonnées (titre, etc.), il faut que vous enregistreriez votre document.
L'option d'enregistrement se situe en haut à droite de l'interface :

![](creer-enr.png)

Attention par défaut toute nouvelle page est en mode _brouillon_ :

![](creer-draft.png)

Si vous souhaitez que cette page soit visible en ligne, il faut passer en "off" le mode brouillon ou _draft_ en anglais :

![](creer-save.png)

Une fois l'enregistrement effectué, le bouton "Save" passe en gris, c'est enregistré et publié. Bravo !

![](creer-publish.png)

**Attention : la page créée ne sera visible que quelques instants plus tard, 1 ou 2 minutes au plus.**

#### Balisage spécifique
Vous pouvez gérer plusieurs paramètres spécifiques :

##### Espaces insécables
Pour ajouter un espace insécable utilisez le code HTML `&nbsp;`, exemple : `Ho un chat&nbsp;!`.

### Modifier les paramètres du livre
Plusieurs choix sont proposés pour paramétrer votre publication, via "Paramètres".

- titre de la publication ;
- sous-titre ;
- auteur ou autrice ;
- licence ;
- maison d'édition.

### Modifier la mise en forme
Quelques options de mise en forme sont proposées pour la version imprimable :

- image de couverture ;
- format de la version imprimable (portrait ou paysage) ;
- afficher ou non les marques de coupe ;
- alignement du texte ;
- retrait de paragraphe ;
- quatrième de couverture ;
- etc.
